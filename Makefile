# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: MPL-2.0

HEADER_FILE := src/common/header.css
USERCHROME_FILES := $(HEADER_FILE) $(sort $(wildcard src/chrome/*.css)) $(sort $(wildcard src/chrome/fonts/*.woff2)) $(sort $(wildcard src/chrome/icons/*.svg)) $(sort $(wildcard src/chrome/icons/old/*.svg))
DESTDIR :=
FIREFOX_DIR := /usr/lib/firefox
FIREFOX_CONFIG_DIR := /etc/firefox

all: out/userChrome.files

clean:
	rm -rf out
out:
	mkdir out

out/userChrome.files: $(USERCHROME_FILES) out
	for i in $(USERCHROME_FILES); do \
        echo "$$i" | cut -d/ -f 2-; \
    done > $@

install: all
	src/prepare_install.sh "$(FIREFOX_DIR)" "$(DESTDIR)"
	install -Dm644 src/policies.json \
		"$(DESTDIR)/$(FIREFOX_CONFIG_DIR)/policies/policies.json"
	install -Dm644 src/chromestyle-config-prefs.js \
		"$(DESTDIR)/$(FIREFOX_DIR)/defaults/pref/chromestyle-config-prefs.js"
	install -Dm644 src/chromestyle-config-autoconfig.js \
		"$(DESTDIR)/$(FIREFOX_DIR)/chromestyle-config-autoconfig.js"
	install -Dm644 "out/userChrome.files" \
		-t "$(DESTDIR)/etc/chromestyle-config-firefox"

	for dir in common chrome; do \
		for i in src/$$dir/*.css; do \
			mkdir -p "$(DESTDIR)/etc/chromestyle-config-firefox/$$dir"; \
			install -Dm644 "$$i" "$(DESTDIR)/etc/chromestyle-config-firefox/$$dir"; \
		done; \
	done; \

	for dir in chrome; do \
		mkdir -p "$(DESTDIR)/etc/chromestyle-config-firefox/$$dir/fonts"; \
		mkdir -p "$(DESTDIR)/etc/chromestyle-config-firefox/$$dir/icons"; \
		mkdir -p "$(DESTDIR)/etc/chromestyle-config-firefox/$$dir/icons/old"; \
		for font in src/$$dir/fonts/*.woff2; do \
			install -Dm644 "$$font" "$(DESTDIR)/etc/chromestyle-config-firefox/$$dir/fonts"; \
		done; \
		for icon in src/$$dir/icons/*.svg; do \
			install -Dm644 "$$icon" "$(DESTDIR)/etc/chromestyle-config-firefox/$$dir/icons"; \
		done; \
		for oldicon in src/$$dir/icons/old/*.svg; do \
			install -Dm644 "$$oldicon" "$(DESTDIR)/etc/chromestyle-config-firefox/$$dir/icons/old"; \
		done; \
	done; \


	install -Dm644 "icons/hicolor/128x128/apps/firefox-esr-chromestyle.png" \
		"$(DESTDIR)/usr/share/icons/hicolor/128x128/apps/firefox-esr-chromestyle.png"; \
	install -Dm644 "icons/hicolor/16x16/apps/firefox-esr-chromestyle.png" \
		"$(DESTDIR)/usr/share/icons/hicolor/16x16/apps/firefox-esr-chromestyle.png"; \
	install -Dm644 "icons/hicolor/24x24/apps/firefox-esr-chromestyle.png" \
		"$(DESTDIR)/usr/share/icons/hicolor/24x24/apps/firefox-esr-chromestyle.png"; \
	install -Dm644 "icons/hicolor/256x256/apps/firefox-esr-chromestyle.png" \
		"$(DESTDIR)/usr/share/icons/hicolor/256x256/apps/firefox-esr-chromestyle.png"; \
	install -Dm644 "icons/hicolor/32x32/apps/firefox-esr-chromestyle.png" \
		"$(DESTDIR)/usr/share/icons/hicolor/32x32/apps/firefox-esr-chromestyle.png"; \
	install -Dm644 "icons/hicolor/48x48/apps/firefox-esr-chromestyle.png" \
		"$(DESTDIR)/usr/share/icons/hicolor/48x48/apps/firefox-esr-chromestyle.png"; \
	install -Dm644 "icons/hicolor/64x64/apps/firefox-esr-chromestyle.png" \
		"$(DESTDIR)/usr/share/icons/hicolor/64x64/apps/firefox-esr-chromestyle.png";

uninstall:
	src/prepare_uninstall.sh "$(FIREFOX_DIR)" "$(DESTDIR)"
	rm -fv "$(DESTDIR)/$(FIREFOX_CONFIG_DIR)/policies/policies.json"
	rm -fv "$(DESTDIR)/$(FIREFOX_DIR)/defaults/pref/chromestyle-config-prefs.js"
	rm -fv "$(DESTDIR)/$(FIREFOX_DIR)/chromestyle-config-autoconfig.js"
	rm -rfv "$(DESTDIR)/etc/chromestyle-config-firefox"
	rm -fv "$(DESTDIR)/usr/share/icons/hicolor/16x16/apps/firefox-esr-chromestyle.png"
	rm -fv "$(DESTDIR)/usr/share/icons/hicolor/24x24/apps/firefox-esr-chromestyle.png"
	rm -fv "$(DESTDIR)/usr/share/icons/hicolor/256x256/apps/firefox-esr-chromestyle.png"
	rm -fv "$(DESTDIR)/usr/share/icons/hicolor/32x32/apps/firefox-esr-chromestyle.png"
	rm -fv "$(DESTDIR)/usr/share/icons/hicolor/48x48/apps/firefox-esr-chromestyle.png"
	rm -fv "$(DESTDIR)/usr/share/icons/hicolor/64x64/apps/firefox-esr-chromestyle.png"

.PHONY: all clean install uninstall
