// Copyright 2023 Oliver Smith, Martijn Braam
// Copyright 2024 Antoni Aloy Torrens
// SPDX-License-Identifier: MPL-2.0

// Set up autoconfig (we use it to copy/update userChrome.css into profile dir)
pref('general.config.filename', "chromestyle-config-autoconfig.js");
pref('general.config.obscure_value', 0);
pref('general.config.sandbox_enabled', false);

// Disable search suggestions
pref('browser.search.suggest.enabled', false);

// Empty new tab page: faster, less distractions
pref('browser.newtabpage.enabled', false);

// Set browser newtab and homepage
pref('browser.startup.homepage', "https://google.com");
pref('browser.newtab.url', "https://google.com");

// Disable browser view
pref('browser.tabs.firefox-view', false);

// Allow UI customizations with userChrome.css and userContent.css
pref('toolkit.legacyUserProfileCustomizations.stylesheets', true);
pref('svg.context-properties.content.enabled', true);
pref('layout.css.color-mix.enabled', true);
